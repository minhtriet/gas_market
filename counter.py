from collections import Counter
import regex as re

def main(filename):
    c = Counter
    words = re.findall(r'\w+', open(filename, encoding='utf-8').read().lower())
    print(Counter(words).most_common(100))

main('2012_spot_trading_analyse.txt')