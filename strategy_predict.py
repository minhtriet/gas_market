import argparse
from datetime import date
from os import path

import numpy as np
import pandas as pd
import tensorflow as tf
import yaml
from numpy import linspace

from util import io

# tf variable
tf_version = '04_11_23_15'
tf_model_path = path.join('logs', 'tf_lstm_%s' % tf_version, 'model.meta')
tf_weight_path = path.join('logs', 'tf_lstm_%s' % tf_version)
sess = tf.Session()
saver = tf.train.import_meta_graph(tf_model_path)
graph = tf.get_default_graph()
saver.restore(sess, tf.train.latest_checkpoint(tf_weight_path))

# news
news = io.load_news('spacy')


def _feed_past_data(market, d, *arguments):
    """
    :param market: the price series
    :param d: current day
    :param arguments: number of days to look back
    :return: news and price data
    """
    if not arguments:
        return market['price'].loc[d]
    else:
        loc = market.index.get_loc(d)
        return market.iloc[loc - arguments[0]:loc]


def should_buy(market, day):
    last_n_days = pd.DataFrame(_feed_past_data(market, day, window), copy=True)
    last_n_days = last_n_days.values[np.newaxis, ...]
    inputs = graph.get_tensor_by_name('input:0')
    output = graph.get_tensor_by_name('output:0')
    prediction_result = sess.run(output, feed_dict={inputs: last_n_days})
    current_day_location = market.index.get_loc(day)
    for i in range(len(prediction_result)):
        log.iloc[current_day_location + i + 1, 'predict'] = min(log.iloc[current_day_location + i + 1, 'predict'], prediction_result[i])


def predict(market):
    # loop by position instead of date to prevent date without data
    for i in range(len(log)):
        should_buy(market, log.index[i].date())
    log.to_csv('predict_lstm.csv', float_format="%.4f")


with open('config.yaml') as stream:
    try:
        config = yaml.load(stream)
        window = config['window']
    except yaml.YAMLError as exc:
        print(exc)

parser = argparse.ArgumentParser(description='buying strategy parsing')
parser.add_argument('--from_day', type=str, help='start buying date', required=True)
parser.add_argument('--to_day', type=str, help='to buying date', required=True)
args = parser.parse_args()

market = io.read_spot_market_v2('gpl')
market = market.join(news, how='left')
market.fillna(0, inplace=True)
market = market[~market.index.duplicated(keep='first')].sort_index()

args.to_day = date(int(args.to_day.split('-')[0]), int(args.to_day.split('-')[1]), int(args.to_day.split('-')[2]))

log = pd.DataFrame(999, index=pd.DatetimeIndex(set(market.loc[args.from_day:args.to_day].index)),
                       columns=['prediction']).sort_index()
predict(market)
